package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
    
    ArrayList<FridgeItem> fridgeItems;
    
    int max_size = 20;
    public Fridge(){
        fridgeItems = new ArrayList<>();
    }

    
    @Override
    public int totalSize(){
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem fridgeItem) {
        if(nItemsInFridge() < max_size){
            fridgeItems.add(fridgeItem);
            return true;
        }
        
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        boolean emp = fridgeItems.isEmpty();
        if (emp == true){
            throw new NoSuchElementException();
        }
           
        else
            fridgeItems.remove(item);
        
    }
    
    @Override
    public void emptyFridge() {
        fridgeItems.clear();
        
    }
    
    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItemsFromFridge = new ArrayList<>();
        ArrayList<FridgeItem> goodItemsFromFridge = new ArrayList<>();
    for(int i = 0; i < fridgeItems.size(); i++ ){
        
       
        FridgeItem fridgeItem = fridgeItems.get(i);
        

        boolean expired = fridgeItem.hasExpired();
        if (expired == true){
            
            expiredItemsFromFridge.add(fridgeItem);
        
        }
        else
        goodItemsFromFridge.add(fridgeItem);

    
    }
    fridgeItems = goodItemsFromFridge;
    return expiredItemsFromFridge;
    }
}
